<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        \DB::table('users')->truncate(); //for cleaning earlier data to avoid duplicate entries
        \DB::table('users')->insert([
          'name' => 'admin',
          'email' => 'admin@admin.com',
          'role' => 'admin',
          'password' => Hash::make('password'),
        ]);
        \DB::table('users')->insert([
          'name' => 'user',
          'email' => 'user@user.com',
          'role' => 'user',
          'password' => Hash::make('password'),
        ]);
      }
}
