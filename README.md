composer install

copy & rename .env.example to .env
create database laravel (default MySQL)

php artisan key:generate
php artisan migrate
php artisan db:seed  
php artisan jwt:secret
 jwt-auth secret [4cRKDBwAhgf6h67mZRK9wcKPemL12aWg2PYirV09RePWDBMCDXC7hEs2tkgFLYsv] set successfully.

download ngrok, & run with port 8000 (default port laravel serve is 8000)

how to test
run postman and this this url with form data
http://127.0.0.1:8000/api/auth/login
form data
    email : admin@admin.com 
    password : password

    email : user@user.com
    password : password

magic command
make controller, model, migration, 
php artisan make:model Employees/Employess -mcr

fake employees
php artisan make:factory PostEmployees --model=Employees/Employees
php artisan tinker

php artisan make:seeder EmployeesTableSeeder