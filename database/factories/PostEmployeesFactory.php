<?php

namespace Database\Factories\Employees;

use App\Models\Employees\Employess;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostEmployeesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Employess::class;
    
    public function definition()
    {
        return [
            'name' => $this->faker->firstName,
            'firstName' => $this->faker->firstName,
            'lastName' => $this->faker->myuserName($this->faker->firstName, $this->faker->firstName),
        ];
    }
}
