<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        $role = \Auth::user()->role; 
        switch ($role) {
          case 'admin':
            return redirect()->route('admin_dashboard')
                        ->with('success','Login created successfully.');
            break;
          case 'user':
            return redirect()->route('user_dashboard')
                        ->with('success','Login created successfully.');
            break; 
      
          default:
            return '/login'; 
          break;
        }
    }
}
