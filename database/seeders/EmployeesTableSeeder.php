<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create('id_ID');
        for($i = 1; $i <= 50; $i++){
            // insert data ke table siswa menggunakan Faker
            \DB::table('employees')->insert([
                'username' => $faker->name,
                'firstName' => $faker->firstName(),
                'lastName' => $faker->lastName(),
                'email' => $faker->email(),
                'birthDate' =>  $faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y-m-d H:i:s'),
                'basicSalary' => $faker->numerify('#####')
            ]);
        }
    }
}

